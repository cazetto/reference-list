### React Ecosystem Tutorials and Resources

#### Pure React Tutorials

- **Best Practices**  

    React Docs - Reconciliation  
    https://reactjs.org/docs/reconciliation.html  

    5 common practices that you can stop doing in React  
    https://blog.logrocket.com/5-common-practices-that-you-can-stop-doing-in-react-9e866df5d269  
    

- **Context API**  
  
    Oficial Documentation  
    https://reactjs.org/docs/context.html  
  
    How to Use New React Context Api detailed  
    https://codeburst.io/how-react-context-api-works-detailed-4fc483d93fd0  
  
    The New React Context API Is Not a Redux Killer  
    https://medium.com/@ExplosionPills/the-new-react-context-api-is-not-a-redux-killer-fadc3c2198c8  
  

#### React With Redux

- **Basics of Redux**  
    
    Oficial Documentation  
    https://redux.js.org/  

    React Redux Tutorial for Beginners: The Definitive Guide (2018)  
    https://www.valentinog.com/blog/react-redux-tutorial-beginners/  
    
- **Resourses for Redux**  
    
    A human-friendly standard for Flux action objects.  
    https://github.com/redux-utilities/flux-standard-action

    Flux Standard Action utilities for Redux.  
    https://github.com/redux-utilities/redux-actions  
    
    FSA-compliant promise middleware for Redux.  
    https://github.com/redux-utilities/redux-promise  
    
    RxJS utilities for Redux.  
    https://github.com/acdlite/redux-rx  

#### Architecture

- **Ducks**  

    Redux: Let’s Code a Higher-Order “Duck”  
    https://medium.com/front-end-hacking/redux-lets-code-a-higher-order-duck-a045415bef0f  

    Scaling your Redux App with ducks  
    https://medium.freecodecamp.org/scaling-your-redux-app-with-ducks-6115955638be  

#### Curiosities  

- **React history**  

    The React Story: How Facebook's Instagram Acquisition Led To The Open Sourcing of React.js  
    https://stackshare.io/posts/the-react-story  
    
